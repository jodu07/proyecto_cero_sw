﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Hotel
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RegistrarHabitacion/{id}/{cat}/{pre}/{est}", ResponseFormat = WebMessageFormat.Json)]
        void RegistrarHabitacion(string id, string cat, string pre, string est);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarHabitacion/{id}/{cat}/{pre}/{est}", ResponseFormat = WebMessageFormat.Json)]
        void ModificarHabitacion(string id, string cat, string pre, string est);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarHabitaciones", ResponseFormat = WebMessageFormat.Json)]
        List<Habitacion> ConsultarHabitaciones();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarHabitacion/{id}", ResponseFormat = WebMessageFormat.Json)]
        Habitacion ConsultarHabitacion(string id);      

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarHabitacion/{id}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarHabitacion(string id);



        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RegistrarHotel/{id}/{nom}/{dir}/{tel}/{adm}/{cat}", ResponseFormat = WebMessageFormat.Json)]
        void RegistrarHotel(string id, string nom, string dir, string tel, string adm, string cat);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarHotel/{id}/{nom}/{dir}/{tel}/{adm}/{cat}", ResponseFormat = WebMessageFormat.Json)]
        void ModificarHotel(string id, string nom, string dir, string tel, string adm, string cat);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarHoteles", ResponseFormat = WebMessageFormat.Json)]
        List<Hotel> ConsultarHoteles();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarHotel/{id}", ResponseFormat = WebMessageFormat.Json)]
        Hotel ConsultarHotel(string id);





        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RegistrarUsuario/{cod}/{nms}/{nom}/{ape}/{ced}/{tel}/{dir}/{cor}/{rol}", ResponseFormat = WebMessageFormat.Json)]
        void RegistrarUsuario(string cod, string nms, string nom, string ape, string ced, string tel, string dir, string cor, string rol);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarUsuario/{cod}/{nms}/{nom}/{ape}/{ced}/{tel}/{dir}/{cor}/{rol}", ResponseFormat = WebMessageFormat.Json)]
        void ModificarUsuario(string cod, string nms, string nom, string ape, string ced, string tel, string dir, string cor, string rol);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarUsuarios", ResponseFormat = WebMessageFormat.Json)]
        List<Usuario> ConsultarUsuarios();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarUsuario/{id}", ResponseFormat = WebMessageFormat.Json)]
        Usuario ConsultarUsuario(string id);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RegistrarReservacion/{cod}/{frn}/{hor}/{fra}/{hbs}", ResponseFormat = WebMessageFormat.Json)]
        void RegistrarReservacion(string cod, string frn, string hor, string fra, string hbs);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarReservacion/{cod}/{frn}/{hor}/{fra}/{hbs}", ResponseFormat = WebMessageFormat.Json)]
        void ModificarReservacion(string cod, string frn, string hor, string fra, string hbs);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarReservaciones", ResponseFormat = WebMessageFormat.Json)]
        List<Reservacion> ConsultarReservaciones();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ConsultarReservacion/{id}", ResponseFormat = WebMessageFormat.Json)]
        Reservacion ConsultarReservacion(string id);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarReservacion/{id}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarReservacion(string id);



        

    


    }
}
