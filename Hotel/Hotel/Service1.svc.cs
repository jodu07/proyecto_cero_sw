﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Hotel
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        HotelContainer context = new HotelContainer();
            public void RegistrarHabitacion(string id, string cat, string pre, string est)
        {

            int x = Convert.ToInt32(id);

            Habitacion entidad = new Habitacion
            {

                Id = x,
                Categoria = cat,
                Precio = pre,
                Estado = est
            };

            context.HabitacionSet.Add(entidad);
            context.SaveChanges();



        }

            public void ModificarHabitacion(string id, string cat, string pre, string est)
            {

                int x = Convert.ToInt32(id);
                var con = (from m in context.HabitacionSet where m.Id == x select m).First();
                con.Categoria = cat;
                con.Precio = pre;
                con.Estado = est;
                context.SaveChanges();

            }



        public List<Habitacion> ConsultarHabitaciones()
        {

            return context.HabitacionSet.ToList();

        }

        public Habitacion ConsultarHabitacion(string id)
        {
            int x = Convert.ToInt32(id);
            return context.HabitacionSet.Where(p => p.Id == x).First();
        }

       

        


        public void EliminarHabitacion(string id)
        {

            int x = Convert.ToInt32(id);
            var con = (from t in context.HabitacionSet where t.Id == x select t).First();
            context.HabitacionSet.Remove(con);
            context.SaveChanges();
        }










        public void RegistrarHotel(string id, string nom, string dir, string tel, string adm, string cat)
        {

            int x = Convert.ToInt32(id);

            Hotel entidad = new Hotel
            {

                Id = x,
                Nombre = nom,
                Direccion = dir,
                Telefono = tel,
                Administrador = adm,
                Categoria = cat,
               
            };

            context.HotelSet.Add(entidad);
            context.SaveChanges();

            

        }

        public void ModificarHotel(string id, string nom, string dir, string tel, string adm, string cat)
        {

            int x = Convert.ToInt32(id);
            var con = (from t in context.HotelSet where t.Id == x select t).First();
            con.Nombre = nom;
            con.Direccion = dir;
            con.Telefono = tel;
            con.Administrador = adm;
            context.SaveChanges();

        }



        public List<Hotel> ConsultarHoteles()
        {

            return context.HotelSet.ToList();

        }

        public Hotel ConsultarHotel(string id)
        {
            int x = Convert.ToInt32(id);
            return context.HotelSet.Where(p => p.Id == x).First();
        }




        public void RegistrarUsuario(string cod, string nms, string nom, string ape, string ced, string tel, string dir, string cor, string rol)
        {

            int x = Convert.ToInt32(cod);

            Usuario entidad = new Usuario
            {

                Codigo= x,
                Nombre_Usuario = nms,
                Nombres = nom,
                Apelllidos = ape,
                Cedula = ced,
                Telefono = tel,
                Direccion = dir,
                Correo_Electronico = cor,
                Rol = rol

            };

            context.UsuarioSet.Add(entidad);
            context.SaveChanges();



        }

        public void ModificarUsuario(string cod, string nms, string nom, string ape, string ced, string tel, string dir, string cor, string rol)
        {

            int x = Convert.ToInt32(cod);
            var con = (from t in context.UsuarioSet where t.Codigo == x select t).First();
                con.Nombre_Usuario = nms;
                con.Nombres = nom;
                con.Apelllidos = ape;
                con.Cedula = ced;
                con.Telefono = tel;
                con.Direccion = dir;
                con.Correo_Electronico = cor;
                con.Rol = rol;


                context.SaveChanges();


        }



        public List<Usuario> ConsultarUsuarios()
        {

            return context.UsuarioSet.ToList();

        }

        public Usuario ConsultarUsuario(string id)
        {
            int x = Convert.ToInt32(id);
            return context.UsuarioSet.Where(p => p.Codigo == x).First();
        }





        public void RegistrarReservacion(string cod, string frn, string hor, string fra, string hbs)
        {

            int x = Convert.ToInt32(cod);

            Reservacion entidad = new Reservacion
            {

                Codigo = x,
                Fecha_Reservacion = frn,
                Hora_Reservacion = hor,
                Fecha_Reserva = fra,
                Habitacion_Solicitada = hbs
                

            };

            context.ReservacionSet.Add(entidad);
            context.SaveChanges();



        }

        public void ModificarReservacion(string cod, string frn, string hor, string fra, string hbs)
        {

            int x = Convert.ToInt32(cod);
            var con = (from t in context.ReservacionSet where t.Codigo == x select t).First();
                con.Codigo = x;
                con.Fecha_Reservacion = frn;
                con.Hora_Reservacion = hor;
                con.Fecha_Reserva = fra;
                con.Habitacion_Solicitada = hbs;


                context.SaveChanges();


        }



        public List<Reservacion> ConsultarReservaciones()
        {

            return context.ReservacionSet.ToList();

        }

        public Reservacion ConsultarReservacion(string id)
        {
            int x = Convert.ToInt32(id);
            return context.ReservacionSet.Where(p => p.Codigo == x).First();
        }

        public void EliminarReservacion(string id)
        {

            int x = Convert.ToInt32(id);
            var con = (from t in context.ReservacionSet where t.Codigo == x select t).First();
            context.ReservacionSet.Remove(con);
            context.SaveChanges();
        }



    }
}
