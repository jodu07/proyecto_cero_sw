
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/27/2015 20:54:29
-- Generated from EDMX file: C:\Users\jorgef\Documents\Visual Studio 2013\sw2\Hotel\Hotel\Hotel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Hotel];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'HabitacionSet'
CREATE TABLE [dbo].[HabitacionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Categoria] nvarchar(max)  NOT NULL,
    [Precio] nvarchar(max)  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'HotelSet'
CREATE TABLE [dbo].[HotelSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Administrador] nvarchar(max)  NOT NULL,
    [Categoria] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UsuarioSet'
CREATE TABLE [dbo].[UsuarioSet] (
    [Codigo] int IDENTITY(1,1) NOT NULL,
    [Nombre_Usuario] nvarchar(max)  NOT NULL,
    [Nombres] nvarchar(max)  NOT NULL,
    [Apelllidos] nvarchar(max)  NOT NULL,
    [Cedula] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Correo_Electronico] nvarchar(max)  NOT NULL,
    [Rol] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ReservacionSet'
CREATE TABLE [dbo].[ReservacionSet] (
    [Codigo] int IDENTITY(1,1) NOT NULL,
    [Fecha_Reservacion] nvarchar(max)  NOT NULL,
    [Hora_Reservacion] nvarchar(max)  NOT NULL,
    [Fecha_Reserva] nvarchar(max)  NOT NULL,
    [Habitacion_Solicitada] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'HabitacionSet'
ALTER TABLE [dbo].[HabitacionSet]
ADD CONSTRAINT [PK_HabitacionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HotelSet'
ALTER TABLE [dbo].[HotelSet]
ADD CONSTRAINT [PK_HotelSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Codigo] in table 'UsuarioSet'
ALTER TABLE [dbo].[UsuarioSet]
ADD CONSTRAINT [PK_UsuarioSet]
    PRIMARY KEY CLUSTERED ([Codigo] ASC);
GO

-- Creating primary key on [Codigo] in table 'ReservacionSet'
ALTER TABLE [dbo].[ReservacionSet]
ADD CONSTRAINT [PK_ReservacionSet]
    PRIMARY KEY CLUSTERED ([Codigo] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------